#include <ESP8266WiFi.h>
#include <WiFiClient.h> 
#include <ESP8266WebServer.h>
#include <WiFiClientSecure.h>
#include <TimeLib.h>
#include <Wire.h>
#include <EEPROM.h> 
// send arduino=true&key=6689305197&temperature=10&humidity=12&state_type=sleep&next_time=1483786448&date_time=1483785448
const char *wfSsid = "ESPap";
const char *wfPassword = "";
ESP8266WebServer server(80);
char ssid[30];
char password[30];
char* host = "eco-craft.herokuapp.com";
String meth = "PATCH"; // !!!
String url = "/plants/1";
String postData = "";
int httpsPort = 443;

//int addr_ssid_l = 50;
//int addr_pass_l = 55;
int addr_ssid = 100;
int addr_pass = 200;

void setup() {
  WiFi.mode(WIFI_STA);
  delay(1000);  
  Serial.begin(115200);
  StartServer();

  EEPROM.begin(512);
  //int s_l = EEPROM.read(addr_ssid_l);
  //int p_l = EEPROM.read(addr_pass_l);
  EEPROM.get(addr_ssid, ssid);
  Serial.println(ssid);  
  EEPROM.get(addr_pass, password);
  Serial.println(password);
  
  EEPROM.end();
  
  WifiConnect();
  Serial.println("start");
}

void loop() {
  server.handleClient();
  ReadSerial();
}

void StartServer() {
  Serial.println();
  Serial.print("Configuring access point...");
  WiFi.softAP(wfSsid, wfPassword);
  IPAddress myIP = WiFi.softAPIP(); 
  Serial.print("AP IP address: ");
  Serial.println(myIP);  http://192.168.4.1 
  
  server.on("/", handleRoot);
  
  server.begin();
  Serial.println("server started");
}

void handleRoot() {
    if(server.hasArg("SSID") || server.hasArg("PASSWORD")){
      String s = server.arg("SSID");
      String p = server.arg("PASSWORD");
      s.toCharArray(ssid, s.length() + 1);
      p.toCharArray(password, p.length() + 1);

      EEPROM.begin(512);
      //EEPROM.write(addr_ssid_l, s.length());
      //EEPROM.write(addr_pass_l, p.length());
      EEPROM.put(addr_ssid, ssid);
      EEPROM.put(addr_pass, password);
      EEPROM.commit();
      EEPROM.end();

      WifiConnect();
    }
    
    else if (server.hasArg("HOST") ) {
      if(server.arg("HOST") == "localhost") {
        String hs = server.arg("HOSTNAME");
        hs.toCharArray(host, hs.length() + 1);
         httpsPort = 3000;
      }
      else {
        host = "eco-craft.herokuapp.com";
         httpsPort = 443;
      }
    }
    
    String oldSsid = ssid;
    String oldPass = password;
    String content = "<html><body><h1>" + oldSsid + " : " + oldPass +"</h1><br>";    
    content += "<form action='/' method='POST'>";
    content += "<table>";
    content += "<tr><td>  Ssid:      </td><td> <input type='text' name='SSID' placeholder='ssid'>             </td></tr>";
    content += "<tr><td>  Password:  </td><td> <input type='text' name='PASSWORD' placeholder='password'> </td></tr>";
    content += "</table><br>";
    content += "<input type='submit' name='SUBMIT' value='Submit'></form><br>";
    
    content += "<br><br>";
    content += "<form action='/' method='POST'>";
    content += "<label><input type='radio' checked name='HOST' value='host' /> Использовать хероку </label>";
    content += "<br>";
    content += "<label><input type='radio' name='HOST' value='localhost' /> Использовать локальный </label>";
    content += "<input type='text' name='HOSTNAME' placeholder='host'>";
    content += "<br><input type='submit' name='SUBMIT' value='Confirm'></form><br>";
     
    server.send(200, "text/html", content);
}

void Send() {  
  WiFiClientSecure client;
  if (!client.connect(host, 443)) {
    Serial.println("connection failed");
    return;
  }

  client.print(meth);
  client.print(" ");
  client.print(url);
  client.print(postData);
  client.println(" HTTP/1.1");
  client.print("Host: ");
  client.println(host);
  client.println("Connection: close");
  client.println();

  while (client.connected()) {
    String line = client.readStringUntil('\n');
    if (line == "\r") {
      break;
    }
  }
  String line = client.readStringUntil('\n');
  Serial.print("json "); Serial.println(line);
}

void WifiConnect() {
  int cnt_upd = 0;
  Serial.print("connecting_to "); Serial.println(ssid);
  if(WiFi.status() != WL_CONNECTED) { WiFi.disconnect(); }
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED && cnt_upd <= 10) {    
      delay(1000);
      cnt_upd++;
      Serial.println(".");
  }
  if(WiFi.status() != WL_CONNECTED && cnt_upd >= 10) {
   // WifiConnect();
  }
  Serial.print("IP_address "); Serial.println(WiFi.localIP());
}

String cmd = "";
void ReadSerial() {
  if(Serial.available()) {
    char c = Serial.read();
    if (c == '\n') {
      ParseCommand(cmd);
      cmd = "";
    } else {
      cmd += c;
    }
  }
}

void ParseCommand(String str) {
  str.trim();
  String p1 = str.substring(0, str.indexOf(" "));
  if(p1 == "send") {  
    String p2 = str.substring(str.indexOf(" ") + 1, str.length());
    postData = "?";
    postData += p2;
    Send();
  } else if(p1 == "check_status") {
    Serial.print("status_connect "); Serial.println(WiFi.status());
    // 1 - no wf
    // 3 - ok
  }
}


