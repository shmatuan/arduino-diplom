#include <ArduinoJson.h> //JSON

#include <DHT.h>
#include <DHT_U.h>
#include <TimeLib.h>
#include <Wire.h>
#include <DS1307RTC.h>
#include <SPI.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define OLED_RESET 4
Adafruit_SSD1306 display(OLED_RESET);
#define DHTPIN 5
DHT dht(DHTPIN, DHT11);

long time_sleep, time_work;
int per_sleep = 30*60;
int per_work = 10;
bool work = false;
bool btn_work = false;

int btn_start_pin = 3;
int rele_pin = 7;

long time_update = 0;
String cmd = "";
long time_send = 0;
long now_time = 0;
bool forseSend = false;

int i_request = 0;

void setup() {
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
  Serial.begin(115200);
  setSyncProvider(RTC.get);
  time_sleep = now() + per_sleep;  
  pinMode(btn_start_pin, INPUT);
  pinMode(rele_pin, OUTPUT);

  time_update = now();
  time_send = now() + 10;
  now_time = now();  
}

void loop() {
  now_time = now();
  
  ReadSerial();
  SendData();
  
  if(time_update <= now_time) {
    time_update += 1;

    UpdateInfo();
    Display();
  }
}

void UpdateInfo() {
  if ( btn_work ) { digitalWrite(rele_pin, HIGH); } 
  else { digitalWrite(rele_pin, LOW); }

  if ( work ) {
     WorkTime();
    if ( time_work <= now_time ) {
      WorkStop();
      if ( !btn_work )SleepTime();
    }
  } else if ( !work ) {
    //SleepTime();
    if ( time_sleep <= now_time ) {
      SleepStop();
      WorkTime();
    }
  }
}

void WorkTime() { 
  digitalWrite(rele_pin, HIGH);
}

void WorkStop() {
  time_sleep = now_time + per_sleep;
  work = false;
  forseSend = true;
}

void SleepTime() {
  digitalWrite(rele_pin, LOW);
}

void SleepStop() {
  time_work = now_time + per_work;
  work = true;
  forseSend = true;
}

int h = 0, m = 0, s = 0;
void Display() {
  h = hour(); m = minute(); s = second();
  
  display.clearDisplay();
  display.setTextColor(WHITE);
  display.setTextSize(1);
  display.setCursor(0,0);
  //display.println(time_send); 
  display.println(digitalClockDisplay()); 
  display.setTextSize(2);
  display.setCursor(0,15);
  display.println(digitalTimerDisplay());
  display.setTextSize(1);
  display.setCursor(75,0);
  display.println(digitalTemperatureDisplay());
  display.setCursor(75,10);
  display.println(digitalHumidityDisplay());
  display.setCursor(75,20);
  int t = per_sleep/60;
  String str1 = "";
  str1 += t;
  str1 +=" : ";
  str1 += per_work;
  display.println(str1);
  display.display();
}

String digitalClockDisplay() {
  String str = "";
  str += h;
  str += ":";
  str += printDigits(m);
  str += ":";
  str += printDigits(s);
  return str;
}

String digitalTimerDisplay() {
  long time_ = 0;
  if ( work ) { time_ = time_work - now_time; }
  else { time_ = time_sleep - now_time; }
  String str = "";
  str += printDigits(time_/60);
  str += ":";
  str += printDigits(time_%60);
  return str;
}

String digitalTemperatureDisplay() {
  String str = "";
  int t = dht.readTemperature();
  str += "T ";
  str += t;
  str += " C";  
  return str;
}

String digitalHumidityDisplay() {
  String str = "";
  int h = dht.readHumidity();
  str += "H ";
  str += h;
  str += " %";  
  return str;
}

String printDigits(int digits){
  String str = "";
  if(digits < 10) str += "0";
  str += digits;
  return str;
}

void ReadSerial() {
  if(Serial.available()) {
    char c = Serial.read();
    if (c == '\n') {
      ParseCommand(cmd);
      cmd = "";
    } else {
      cmd += c;
    }
  }
}

void SendData() {
  if( time_send <= now_time || forseSend )
  {
    if(forseSend || work) time_send = now_time + 1;
    else  time_send = now_time + 10;
    forseSend = false;    
    
    String str = "";
    str += "send ";
    str += "key=6689305197";
    str += "&arduino=true";
    str += "&temperature=";
    str += dht.readTemperature();
    str += "&humidity=";
    str += dht.readHumidity();
    str += "&date_time=";
    str += now_time;
    if ( work ) { 
      str += "&state_type=work";
      str += "&next_time=";
      str += time_work;
    }
    else {
      str += "&state_type=sleep";
      str += "&next_time=";
      str += time_sleep;
    }
    Serial.println(str);
  }
}

void ParseCommand(String str) {
  str.trim();
  String p1 = str.substring(0, str.indexOf(" "));
  String p2 = str.substring(str.indexOf(" ") + 1, str.length());

  if(p1 == "json") {
    StaticJsonBuffer<100> jsonBuffer;
    JsonObject& root = jsonBuffer.parseObject(p2);
    if (!root.success())
    {
      return;
    }
    else {
      //if(root["instructions"]) {
        i_request++;
        String p_s = root["info"][0];
        String p_w = root["info"][1];
        if( per_sleep != p_s.toInt() ) {
          per_sleep = p_s.toInt();
          if(!work) {
             time_sleep = now_time + per_sleep;
          }
        }
        if( per_work  != p_w.toInt() ) {
          per_work = p_w.toInt();
          if(work){
            time_work = now_time + per_work;
          }          
        }
      //}
    }
   }
   else if (p1 == "IP_address" ) {
   }
}
